#
# A very basic phppgadmin docker image
#
# https://github.com/naturalis/phppgadmin-docker/blob/edit_config_using_getenv/Dockerfile

FROM php:7.4-alpine

LABEL description="A very basic phppgadmin docker image"
LABEL maintainer="andersevenrud@gmail.com"
LABEL version="7.14.6-mod"

ARG PGADMIN_VERSION=7.14.6-mod

ENV PGADMIN_NAME pgadmin
ENV PGADMIN_HOSTNAME localhost
ENV PGADMIN_PORT 5432

RUN apk add --no-cache wget postgresql-dev

RUN docker-php-ext-install pgsql pdo pdo_pgsql

RUN wget https://github.com/ReimuHakurei/phpPgAdmin/archive/refs/tags/v${PGADMIN_VERSION}.zip && \
    unzip v${PGADMIN_VERSION}.zip

WORKDIR "phpPgAdmin-${PGADMIN_VERSION}"

COPY config.inc.php conf/

CMD php -S 0.0.0.0:8080
